import React from "react";
import "./index.css";
class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ca: "",
            name: "",
            captain: false
        }
    }

    onCategory(e) {
        console.log(e.target.value)
        this.setState({ ca: e.target.value });
    }
    onName(e) {
        console.log(e.target.value)

        this.setState({ name: e.target.value });
    }
    onCheck(e) {
        console.log(e.target.checked);
        this.setState({ captain: e.target.checked });
    }
    onAdd(e) {
        e.preventDefault();
        const { name, ca, captain } = this.state;
        if (!name) {
            alert('name cannot be empty');
            return;
        }
        else if (!ca) {
            alert('category cannot be empty');
            return;
        }
        this.props.addPerson({name,ca,captain});
    }


    render() {
        return (
            <form onSubmit={(e) => this.onAdd(e)} className="menu">
                <div className="name" >
                    <span> Player Name: </span>
                    <input type="input" onChange={(e) => this.onName(e)} />

                </div>
                <div className="category" >
                    <span> Category</span>
                    <select onChange={(e) => this.onCategory(e)} >
                        <option> {""} </option>
                        <option> Batsman </option>
                        <option> Bowler </option>
                        <option> Wicket Keeper </option>
                        <option> All Rounder </option>
                    </select>
                </div>
                <div className="captain" >
                    <span>captain:</span>
                    <input type="checkbox" onChange={(e) => this.onCheck(e)} />
                </div>
                <button>Add</button>
            </form>
        )
    }
}

export default Menu;