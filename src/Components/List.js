import React from "react";
import "./List.css";
class List extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        if(this.props.arr.length === 0)
            return "";

        return (
            <div className="List">
                {this.props.ca}
                <li>
                    {
                        this.props.arr.map(i => {
                            return <ul key={i.name} > {i.name}  </ul>
                        })                        
                    }
                </li>
            </div>
        )
    }
}

export default List;