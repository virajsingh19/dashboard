import React from "react";
import Menu from "./Menu";
import List from "./List";
import "./Container.css"

class Store extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ba: [],
            bo: [],
            wk: [],
            ar: []
        }
    }

    onAdd(person) {
        console.log("person is", person);
        const { ca } = person;

        switch (ca) {
            case "Batsman":
                this.setState({ ba: [...this.state.ba, person] })
                return;
            case "Bowler":
                this.setState({ bo: [...this.state.bo, person] })
                return;
            case "Wicket Keeper":
                this.setState({ wk: [...this.state.wk, person] })
                return;
            case "All Rounder":
                this.setState({ ar: [...this.state.ar, person] })
                return;
            default:
                return;
        }
    }


    render() {
        return (
            <React.Fragment>
                <Menu addPerson={this.onAdd.bind(this)} />
                <div className="view">
                    <List ca="Batsman" arr={this.state.ba} />
                    <List ca="Bowler" arr={this.state.bo} />
                    <List ca="Wicket Keeper" arr={this.state.wk} />
                    <List ca="All Rounder" arr={this.state.ar} /> 
                </div>
            </React.Fragment>
        );
    }
}

export default Store;